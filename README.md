# What's cooking? #


Repositorio para el reto de [www.kaggle.com](www.kaggle.com)


### Estructura ###

Cada modelo construido tiene un fichero propio donde se realizado la busqueda de parametros adecuados además
de realizar los cross validation necesarios para cada uno de ellos.

Los script run.py y run_metaclassifiers.py ejecutan para el test los modelos con los mejores parametros obtenidos en
la etapa anterior.