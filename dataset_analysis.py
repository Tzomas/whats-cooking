import pandas as pd
import matplotlib.pyplot as plot
from collections import Counter
import numpy as np
from scipy import stats

train = pd.read_json('/users/tomas/PycharmProjects/whats-cooking/datasets/train.json')
test = pd.read_json('/users/tomas/PycharmProjects/whats-cooking/datasets/test.json')

samples_types = train['cuisine']
counter_cuisines = Counter(samples_types)

cuisine_dataframe = pd.DataFrame.from_dict(data=counter_cuisines, orient='index')
cuisine_dataframe.columns = ['Repeticiones']
cuisine_dataframe.to_csv('/users/tomas/PycharmProjects/whats-cooking/datasets/cuisine_counter.csv')

cuisine_dataframe.plot(kind='bar', legend=True, sort_columns=True, title="Número de samples en el dataset de entrenamiento")
plot.show()

lens_train = np.array([len(x) for x in train['ingredients']])
print('Estudio ingredientes train')
print('Número mínimo:', lens_train.min())
print('Número máximo:', lens_train.max())
print('Número medio:', lens_train.mean())
print('Moda:',stats.mode(lens_train).mode)
lens_test = np.array([len(x) for x in test['ingredients']])
print('Estudio ingredientes test')
print('Número mínimo:', lens_test.min())
print('Número máximo:', lens_test.max())
print('Número medio:', lens_test.mean())
print('Moda:',stats.mode(lens_test).mode)

