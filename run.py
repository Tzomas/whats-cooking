import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC, LinearSVC

train = pd.read_json('/users/tomas/PycharmProjects/whats-cooking/datasets/train.json')
train_x = [' '.join(ingredients).lower().strip() for ingredients in train['ingredients']]
train_y = train['cuisine']
test = pd.read_json('/users/tomas/PycharmProjects/whats-cooking/datasets/test.json')
test_x = [' '.join(ingredients).lower().strip() for ingredients in test['ingredients']]

tfidf = TfidfVectorizer(stop_words='english', binary=True)

print('Logistic Regression classifiers')
lr = LogisticRegression(C=10, tol=0.0001)
model = Pipeline([('trans', tfidf), ('clf', lr)])
model.fit(train_x, train_y)

predict = model.predict(test_x)
test['cuisine'] = predict
test[['id', 'cuisine']].to_csv("/users/tomas/PycharmProjects/whats-cooking/submissions/lr10-0001submission.csv", index=False)

lr = LogisticRegression(C=10, tol=0.001)
model = Pipeline([('trans', tfidf), ('clf', lr)])
model.fit(train_x, train_y)

predict = model.predict(test_x)
test['cuisine'] = predict
test[['id', 'cuisine']].to_csv("/users/tomas/PycharmProjects/whats-cooking/submissions/lr10-001submission.csv", index=False)

lr = LogisticRegression(C=10, tol=0.01)
model = Pipeline([('trans', tfidf), ('clf', lr)])
model.fit(train_x, train_y)

predict = model.predict(test_x)
test['cuisine'] = predict
test[['id', 'cuisine']].to_csv("/users/tomas/PycharmProjects/whats-cooking/submissions/lr10-01submission.csv", index=False)

lr = LogisticRegression(C=10, tol=0.001, solver='lbfgs')
model = Pipeline([('trans', tfidf), ('clf', lr)])
model.fit(train_x, train_y)

predict = model.predict(test_x)
test['cuisine'] = predict
test[['id', 'cuisine']].to_csv("/users/tomas/PycharmProjects/whats-cooking/submissions/lrlbfgs10-001submission.csv", index=False)

lr = LogisticRegression(C=10, tol=0.01, solver='lbfgs')
model = Pipeline([('trans', tfidf), ('clf', lr)])
model.fit(train_x, train_y)

predict = model.predict(test_x)
test['cuisine'] = predict
test[['id', 'cuisine']].to_csv("/users/tomas/PycharmProjects/whats-cooking/submissions/lrlbfgs10-01submission.csv", index=False)


print('Linear SVC classifiers')
lsvc = LinearSVC(C=1, tol=0.1, penalty='l2')
model = Pipeline([('trans', tfidf), ('clf', lsvc)])
model.fit(train_x, train_y)

predict = model.predict(test_x)
test['cuisine'] = predict
test[['id', 'cuisine']].to_csv("/users/tomas/PycharmProjects/whats-cooking/submissions/lsvc1-1submission.csv", index=False)

lsvc = LinearSVC(C=1, tol=0.01, penalty='l2')
model = Pipeline([('trans', tfidf), ('clf', lsvc)])
model.fit(train_x, train_y)

predict = model.predict(test_x)
test['cuisine'] = predict
test[['id', 'cuisine']].to_csv("/users/tomas/PycharmProjects/whats-cooking/submissions/lsvc1-01submission.csv", index=False)

lsvc = LinearSVC(C=1, tol=0.001, penalty='l2')
model = Pipeline([('trans', tfidf), ('clf', lsvc)])
model.fit(train_x, train_y)

predict = model.predict(test_x)
test['cuisine'] = predict
test[['id', 'cuisine']].to_csv("/users/tomas/PycharmProjects/whats-cooking/submissions/lsvc1-001submission.csv", index=False)

lsvc = LinearSVC(C=1, tol=0.0001, penalty='l2')
model = Pipeline([('trans', tfidf), ('clf', lsvc)])
model.fit(train_x, train_y)

predict = model.predict(test_x)
test['cuisine'] = predict
test[['id', 'cuisine']].to_csv("/users/tomas/PycharmProjects/whats-cooking/submissions/lsvc1-0001submission.csv", index=False)

print('SVC classifiers')
svc = SVC(C=10, kernel='rbf', gamma=1, coef0=1, tol=0.001)
model = Pipeline([('trans', tfidf), ('clf', svc)])
model.fit(train_x, train_y)

predict = model.predict(test_x)
test['cuisine'] = predict
test[['id', 'cuisine']].to_csv("/users/tomas/PycharmProjects/whats-cooking/submissions/svc10-001submission.csv", index=False)

svc = SVC(C=10, kernel='rbf', gamma=1, coef0=1, tol=0.01)
model = Pipeline([('trans', tfidf), ('clf', svc)])
model.fit(train_x, train_y)

predict = model.predict(test_x)
test['cuisine'] = predict
test[['id', 'cuisine']].to_csv("/users/tomas/PycharmProjects/whats-cooking/submissions/svc10-01submission.csv", index=False)

svc = SVC(C=100, kernel='rbf', gamma=1, coef0=1, tol=0.001)
model = Pipeline([('trans', tfidf), ('clf', svc)])
model.fit(train_x, train_y)

predict = model.predict(test_x)
test['cuisine'] = predict
test[['id', 'cuisine']].to_csv("/users/tomas/PycharmProjects/whats-cooking/submissions/svc100-001submission.csv", index=False)

svc = SVC(C=100, kernel='rbf', gamma=1, coef0=1, tol=0.01)
model = Pipeline([('trans', tfidf), ('clf', svc)])
model.fit(train_x, train_y)

predict = model.predict(test_x)
test['cuisine'] = predict
test[['id', 'cuisine']].to_csv("/users/tomas/PycharmProjects/whats-cooking/submissions/svc100-01submission.csv", index=False)

