import pandas as pd

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble.voting_classifier import VotingClassifier
from sklearn.multiclass import OneVsRestClassifier

train = pd.read_json('/users/tomas/PycharmProjects/whats-cooking/datasets/train.json')
train_x = [' '.join(ingredients).lower().strip() for ingredients in train['ingredients']]
train_y = train['cuisine']
test = pd.read_json('/users/tomas/PycharmProjects/whats-cooking/datasets/test.json')
test_x = [' '.join(ingredients).lower().strip() for ingredients in test['ingredients']]

#Modelo Voting
tfidf = TfidfVectorizer(stop_words='english', binary=True)
lr = LogisticRegression(C=10, tol=0.01)
lr_v = Pipeline([('trans', tfidf), ('clf', lr)])

lsvc = LinearSVC(C=1, tol=0.1, penalty='l2')
lsvc_v = Pipeline([('trans', tfidf), ('clf', lsvc)])

svc = SVC(C=100, kernel='rbf', gamma=1, tol=0.001)
svc_v = Pipeline([('trans', tfidf), ('clf', svc)])

voting = VotingClassifier(estimators=[('svc', lsvc_v),('svc', svc_v)])
voting.fit(train_x, train_y)

predict = voting.predict(test_x)
test['cuisine'] = predict
test[['id', 'cuisine']].to_csv("/users/tomas/PycharmProjects/whats-cooking/submissions/voting2-hardsubmission.csv", index=False)

#Modelo OneVsRest
ovr = OneVsRestClassifier(svc_v,n_jobs=4)
ovr.fit(train_x,train_y)
predict = ovr.predict(test_x)
test['cuisine'] = predict
test[['id', 'cuisine']].to_csv("/users/tomas/PycharmProjects/whats-cooking/submissions/ovr-submission.csv", index=False)
