import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
from sklearn.grid_search import GridSearchCV

train = pd.read_json('/users/tomas/PycharmProjects/whats-cooking/datasets/train.json')
train_x = [' '.join(ingredients).lower().strip() for ingredients in train['ingredients']]
train_y = train['cuisine']

#Modelo Support Vector Classifier
tfidf = TfidfVectorizer(stop_words='english', binary=True)
svc = SVC()
model = Pipeline([('trans', tfidf), ('clf', svc)])

params = [{'clf__C': [10, 100], 'clf__tol': [0.01, 0.001],
           'clf__kernel': ['rbf', 'poly', 'sigmoid'],'clf__gamma':[0, 1]}]

print('Ejecutando GridSearch')
gs = GridSearchCV(model, param_grid=params, cv=3, n_jobs=3, verbose=1)
gs.fit(train_x[:5000], train_y[:5000])

print('Parametros')
best_five = sorted(gs.grid_scores_, key=lambda x: x.mean_validation_score, reverse=True)[:5]
print(best_five)

print('Mejor parametros')
print(gs.best_params_)

print('Mejor score')
print(gs.best_score_)
