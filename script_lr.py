import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.grid_search import GridSearchCV

print('Cargando corpus')
train = pd.read_json('/users/tomas/PycharmProjects/whats-cooking/datasets/train.json')
train_x = [' '.join(ingredients).lower().strip() for ingredients in train['ingredients']]
train_y = train['cuisine']

# Modelo Logistic Regression
tfidf = TfidfVectorizer(stop_words='english', binary=True)
lg = LogisticRegression()
model = Pipeline([('trans', tfidf), ('clf', lg)])

params = [{'clf__C': [0.1, 1, 10, 100, 1000], 'clf__tol': [0.1, 0.01, 0.001, 0.0001]},
          {'clf__C': [0.1, 1, 10, 100, 1000], 'clf__tol': [0.1, 0.01, 0.001, 0.0001],
           'clf__solver': ['lbfgs'], 'clf__multi_class': ['ovr', 'multinomial']}]

gs = GridSearchCV(model, param_grid=params, cv=3, n_jobs=3, verbose=1)
gs.fit(train_x, train_y)

print('Parametros')
best_five = sorted(gs.grid_scores_, key=lambda x: x.mean_validation_score, reverse=True)[:5]
print(best_five)

print('Mejor parametros')
print(gs.best_params_)

print('Mejor score')
print(gs.best_score_)
